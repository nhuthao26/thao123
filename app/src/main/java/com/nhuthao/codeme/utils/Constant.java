package com.nhuthao.codeme.utils;

public class Constant {
    public static final String POST_ID = "post_id_key";
    public static final String POST_NUMBER_LIKE = "post_number_like_key";
    public static final String POST_IS_LIKE = "post_is_like_key";

    public static final String IS_LIKE = "is_like";
    public static final String NUMBER_LIKE = "number_like";
    public static final String NUMBER_COMMENT = "number_comment";

    public static final int REQUEST_CODE_COMMENT = 26;
}
