package com.nhuthao.codeme.network.Youtube;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitYoutube {

    private Retrofit retrofit;
    private static RetrofitYoutube instance;

    private RetrofitYoutube(){
        retrofit = new Retrofit.Builder()
                .baseUrl(YoutubeAPI.ROOT)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public static RetrofitYoutube getIntance(){
        if(instance==null){
            instance = new RetrofitYoutube();
        }
        return instance;
    }

    public YoutubeRetrofitService createService(){
        return retrofit.create(YoutubeRetrofitService.class);
    }
}
