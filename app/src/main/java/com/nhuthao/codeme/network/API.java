package com.nhuthao.codeme.network;

public class API {
    public static final String ROOT = "https://hubbook.herokuapp.com/";

    //Router
    static final String LOGIN = "api/user/login";
    static final String REGISTER = "api/user/register";
    static final String GET_ALL_STATUS = "api/post/get-all";
    static final String LIKE_STATUS = "api/post/like";
    static final String CREAT_STATUS = "api/post";
    static final String GET_PROFILE = "api/user/get-detail";
    static final String GET_MY_GROUP = "api/chat/all-group";
    static final String GET_FRIENDS = "api/user/get-all";
    static final String GET_ALL_COMMENT = "api/post/get-comment";
    static final String POST_COMMENT = "api/post/comment";
    static final String CREAT_GROUP_CHAT = "api/chat/create-group";
    static final String GET_ALL_MESSAGE = "api/chat/all-message";
    static final String UPDATE_AVATAR = "api/user/update-avatar";
}
