package com.nhuthao.codeme.network;

import com.nhuthao.codeme.Model.request.CommentSendForm;
import com.nhuthao.codeme.Model.request.CreatGroupChatSendForm;
import com.nhuthao.codeme.Model.request.CreatStatusSendForm;
import com.nhuthao.codeme.Model.request.LikeSendForm;
import com.nhuthao.codeme.Model.request.LoginSendForm;
import com.nhuthao.codeme.Model.request.RegisterSendForm;
import com.nhuthao.codeme.Model.request.UpdateAvatarSendForm;
import com.nhuthao.codeme.Model.response.AvatarUrl;
import com.nhuthao.codeme.Model.response.GroupChatResponse;
import com.nhuthao.codeme.Model.response.Groupchat;
import com.nhuthao.codeme.Model.response.Message;
import com.nhuthao.codeme.Model.response.Profile;
import com.nhuthao.codeme.Model.response.Status;
import com.nhuthao.codeme.Model.response.UserComment;
import com.nhuthao.codeme.Model.response.UserInfo;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Query;


public interface RetrofitService {

    @POST(API.LOGIN)
    Call<UserInfo> login(@Body LoginSendForm sendForm);

    @POST(API.REGISTER)
    Call<UserInfo> Register(@Body RegisterSendForm registerSendForm);

    @GET(API.GET_ALL_STATUS)
    Call<ArrayList<Status>> getAllStatus(@Query("userId") String userId);

    @POST(API.LIKE_STATUS)
    Call<Void> LikeStatus(@Body LikeSendForm sendForm);

    @POST(API.CREAT_STATUS)
    Call<Status> CreatStatus(@Body CreatStatusSendForm sendForm);

    @GET(API.GET_PROFILE)
    Call<Profile> getProfile(@Query("username") String username, @Header("userId") String userId);

    @GET(API.GET_MY_GROUP)
    Call<ArrayList<Groupchat>> getMychat(@Query("userId") String userId);

    @GET(API.GET_FRIENDS)
    Call<ArrayList<UserInfo>> getFriends(@Query("userId") String userId);

    @GET(API.GET_ALL_COMMENT)
    Call<ArrayList<UserComment>> getAllComment(@Query("postId") String postId);

    @POST(API.POST_COMMENT)
    Call<UserComment> postComment(@Body CommentSendForm sendForm);

    @POST(API.CREAT_GROUP_CHAT)
    Call<GroupChatResponse> CreatGroupChat(@Body CreatGroupChatSendForm sendForm);

    @GET(API.GET_ALL_MESSAGE)
    Call<ArrayList<Message>> getAllMessage(@Query("groupId") String groupId);

    @PUT(API.UPDATE_AVATAR)
    Call<AvatarUrl> updateAvatar(@Body AvatarUrl sendForm, @Query("userId") String userId);
}
