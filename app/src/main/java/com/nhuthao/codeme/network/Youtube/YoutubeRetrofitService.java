package com.nhuthao.codeme.network.Youtube;

import com.nhuthao.codeme.Model.response.YoutubeResponce.Channel;
import com.nhuthao.codeme.Model.response.YoutubeResponce.Item;

import java.util.ArrayList;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Part;
import retrofit2.http.Query;

public interface YoutubeRetrofitService {

    @GET(YoutubeAPI.GETLIST)
    Call<Channel> GetAllVideo(@Query("part") String part,
                              @Query("playlistId") String playlistId,
                              @Query("key") String key);

//    @GET(YoutubeAPI.GETLIST)
//    Call<ArrayList<Item>> GetAllVideo(
//                                      @Query("playlistId") String playlistId,
//                                      @Query("key") String key);
}
