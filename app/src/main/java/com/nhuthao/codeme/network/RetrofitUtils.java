package com.nhuthao.codeme.network;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitUtils {

    private Retrofit retrofit;
    private static RetrofitUtils instance;

    private RetrofitUtils(){
        retrofit = new Retrofit.Builder()
                .baseUrl(API.ROOT)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public static RetrofitUtils getIntance(){
        if(instance==null){
            instance = new RetrofitUtils();
        }
        return instance;
    }

    public RetrofitService createService(){
        return retrofit.create(RetrofitService.class);
    }

//    public <T> T createService(Class<T> service){
//        return retrofit.create(service);
//    }
}
