package com.nhuthao.codeme.Model.response.YoutubeResponce;

import com.google.gson.annotations.SerializedName;

public class PageInfo {
    @SerializedName("totalResults")
    int totalResults;

    @SerializedName("resultsPerPage")
    int resultsPerPage;

    public PageInfo(int totalResults, int resultsPerPage) {
        this.totalResults = totalResults;
        this.resultsPerPage = resultsPerPage;
    }

    public int getTotalResults() {
        return totalResults;
    }

    public void setTotalResults(int totalResults) {
        this.totalResults = totalResults;
    }

    public int getResultsPerPage() {
        return resultsPerPage;
    }

    public void setResultsPerPage(int resultsPerPage) {
        this.resultsPerPage = resultsPerPage;
    }
}
