package com.nhuthao.codeme.Model.response.YoutubeResponce;

import com.google.gson.annotations.SerializedName;

public class Thumbnails {

    @SerializedName("default")
    ImageIntro defaults;

    @SerializedName("medium")
    ImageIntro medium;

    @SerializedName("high")
    ImageIntro high;

    public Thumbnails(ImageIntro defaults, ImageIntro medium, ImageIntro high) {
        this.defaults = defaults;
        this.medium = medium;
        this.high = high;
    }

    public ImageIntro getDefaults() {
        return defaults;
    }

    public void setDefaults(ImageIntro defaults) {
        this.defaults = defaults;
    }

    public ImageIntro getMedium() {
        return medium;
    }

    public void setMedium(ImageIntro medium) {
        this.medium = medium;
    }

    public ImageIntro getHigh() {
        return high;
    }

    public void setHigh(ImageIntro high) {
        this.high = high;
    }

//    public ImageIntro getStandard() {
//        return standard;
//    }
//
//    public void setStandard(ImageIntro standard) {
//        this.standard = standard;
//    }
}
