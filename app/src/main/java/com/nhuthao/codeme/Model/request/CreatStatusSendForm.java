package com.nhuthao.codeme.Model.request;

import com.google.gson.annotations.SerializedName;

public class CreatStatusSendForm {
    @SerializedName("userId")
    String userId;
    @SerializedName("content")
    String content;


    public CreatStatusSendForm(String userId, String content) {
        this.userId = userId;
        this.content = content;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
