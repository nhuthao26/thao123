package com.nhuthao.codeme.Model.request;

import com.google.gson.annotations.SerializedName;

public class CommentSendForm {

    @SerializedName("userId")
    String userId;

    @SerializedName("postId")
    String postId;

    @SerializedName("content")
    String content;

    public CommentSendForm(String userId, String postId, String content) {
        this.userId = userId;
        this.postId = postId;
        this.content = content;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getPostId() {
        return postId;
    }

    public void setPostId(String postId) {
        this.postId = postId;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}

