package com.nhuthao.codeme.Model.request;

import com.google.gson.annotations.SerializedName;

public class UpdateAvatarSendForm {
    @SerializedName("avatarUrl")
    String avatarUrl;

    public UpdateAvatarSendForm(String avatarUrl) {

    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }
}
