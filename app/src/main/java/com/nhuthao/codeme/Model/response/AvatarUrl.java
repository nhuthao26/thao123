package com.nhuthao.codeme.Model.response;

import com.google.gson.annotations.SerializedName;

public class AvatarUrl {
    @SerializedName("avatarUrl")
    private String avatarUrl;

    public AvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }

    public String getAvatarUrl() {
        return avatarUrl;
    }

    public void setAvatarUrl(String avatarUrl) {
        this.avatarUrl = avatarUrl;
    }
}
