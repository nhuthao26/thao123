package com.nhuthao.codeme.Model.response.YoutubeResponce;

import com.google.gson.annotations.SerializedName;

public class Snippet {
    @SerializedName("title")
    String title;

    @SerializedName("thumbnails")
    Thumbnails thumbnails;

    public Snippet(String title, Thumbnails thumbnails) {
        this.title = title;
        this.thumbnails = thumbnails;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Thumbnails getThumbnails() {
        return thumbnails;
    }

    public void setThumbnails(Thumbnails thumbnails) {
        this.thumbnails = thumbnails;
    }
}
