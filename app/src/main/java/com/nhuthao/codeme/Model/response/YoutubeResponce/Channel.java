package com.nhuthao.codeme.Model.response.YoutubeResponce;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Channel {
    @SerializedName("pageInfo")
    PageInfo pageInfo;

    @SerializedName("items")
    ArrayList<Item> items;

    public Channel(PageInfo pageInfo, ArrayList<Item> items) {
        this.pageInfo = pageInfo;
        this.items = items;
    }

    public PageInfo getPageInfo() {
        return pageInfo;
    }

    public void setPageInfo(PageInfo pageInfo) {
        this.pageInfo = pageInfo;
    }

    public ArrayList<Item> getItems() {
        return items;
    }

    public void setItems(ArrayList<Item> items) {
        this.items = items;
    }
}
