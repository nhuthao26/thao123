package com.nhuthao.codeme.Model.response;

import com.google.gson.annotations.SerializedName;

public class UserComment {

    @SerializedName("userAvatar")
    String userAvatar;

    @SerializedName("username")
    String username;

    @SerializedName("fullName")
    String fullName;

    @SerializedName("content")
    String content;

    public String getUserAvatar() {
        return userAvatar;
    }

    public void setUserAvatar(String userAvatar) {
        this.userAvatar = userAvatar;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
}
