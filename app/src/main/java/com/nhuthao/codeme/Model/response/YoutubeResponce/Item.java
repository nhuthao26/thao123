package com.nhuthao.codeme.Model.response.YoutubeResponce;

import com.google.gson.annotations.SerializedName;

public class Item {
    @SerializedName("id")
    String id;

    @SerializedName("snippet")
    Snippet snippet;

    public Item(String id, Snippet snippet) {
        this.id = id;
        this.snippet = snippet;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Snippet getSnippet() {
        return snippet;
    }

    public void setSnippet(Snippet snippet) {
        this.snippet = snippet;
    }
}
