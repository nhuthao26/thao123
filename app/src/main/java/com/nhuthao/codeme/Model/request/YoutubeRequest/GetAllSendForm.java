package com.nhuthao.codeme.Model.request.YoutubeRequest;

import com.google.gson.annotations.SerializedName;

public class GetAllSendForm {

    @SerializedName("part")
    String part;

    @SerializedName("playlistId")
    String playlistId;

    @SerializedName("key")
    String key;

    public GetAllSendForm(String part, String playlistId, String key) {
        this.part = part;
        this.playlistId = playlistId;
        this.key = key;
    }

    public String getPart() {
        return part;
    }

    public void setPart(String part) {
        this.part = part;
    }

    public String getPlaylistId() {
        return playlistId;
    }

    public void setPlaylistId(String playlistId) {
        this.playlistId = playlistId;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
