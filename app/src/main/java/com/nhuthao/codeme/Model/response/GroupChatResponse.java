package com.nhuthao.codeme.Model.response;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class GroupChatResponse {
    @SerializedName("name")
    String name;

    @SerializedName("users")
    ArrayList<String> users;

    @SerializedName("_id")
    String _id;

    @SerializedName("messages")
    ArrayList<Message> messages;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ArrayList<String> getUsers() {
        return users;
    }

    public void setUsers(ArrayList<String> users) {
        this.users = users;
    }

    public String get_id() {
        return _id;
    }

    public void set_id(String _id) {
        this._id = _id;
    }

    public ArrayList<Message> getMessages() {
        return messages;
    }

    public void setMessages(ArrayList<Message> messages) {
        this.messages = messages;
    }
}
