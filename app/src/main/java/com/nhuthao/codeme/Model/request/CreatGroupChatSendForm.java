package com.nhuthao.codeme.Model.request;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class CreatGroupChatSendForm {
    @SerializedName("users")
    ArrayList<String> user;

    public CreatGroupChatSendForm(ArrayList<String> user) {
        this.user = user;
    }

    public ArrayList<String> getUser() {
        return user;
    }

    public void setUser(ArrayList<String> user) {
        this.user = user;
    }
}
