package com.nhuthao.codeme.Model.response.YoutubeResponce;

import com.google.gson.annotations.SerializedName;

public class ImageIntro {

    @SerializedName("url")
    String url;

    @SerializedName("width")
    int width;

    @SerializedName("height")
    int height;

    public ImageIntro(String url, int width, int height) {
        this.url = url;
        this.width = width;
        this.height = height;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }
}
