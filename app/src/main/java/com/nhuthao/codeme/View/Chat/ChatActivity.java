package com.nhuthao.codeme.View.Chat;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;
import com.google.gson.Gson;
import com.nhuthao.codeme.Model.response.Message;
import com.nhuthao.codeme.Model.response.UserInfo;
import com.nhuthao.codeme.R;
import com.nhuthao.codeme.View.Chat.Adapter.ChatAdapter;
import com.nhuthao.codeme.base.BaseActivity;
import com.nhuthao.codeme.database.RealmContext;
import com.nhuthao.codeme.network.API;
import com.nhuthao.codeme.network.RetrofitService;
import com.nhuthao.codeme.network.RetrofitUtils;

import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ChatActivity extends BaseActivity {

    public static final String GROUP_ID = "KEY_GroupId";
    public static final int REQ_CODE_SPEECH_INPUT = 1;

    @BindView(R.id.rcv_chat)
    RecyclerView rcvchat;

    @BindView(R.id.edt_message)
    EditText edtchat;

    @BindView(R.id.imv_backChat)
    ImageView back;

    @BindView(R.id.imv_message)
    ImageView sendchat;

    @BindView(R.id.imv_mic)
    ImageView mic;

    private ArrayList<Message> messages;
    RetrofitService retrofitService;
    ChatAdapter chatAdapter;
    UserInfo userInfo;
    String groupId;

    private Socket socket;
    {
        try {
            socket = IO.socket(API.ROOT);
        } catch (URISyntaxException e) {}
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);
        ButterKnife.bind(this);
        init();
        addListener();
    }

    private void init() {
        retrofitService = RetrofitUtils.getIntance().createService();
        userInfo = RealmContext.getInstance().getAll();
        groupId = getIntent().getSerializableExtra(GROUP_ID).toString();

        socket.connect();
        socket.emit("join_chat", groupId, userInfo.getId());
        socket.on("new_message", onNewMessageListener);

        messages = new ArrayList<>();
        chatAdapter = new ChatAdapter(messages);
        rcvchat.setAdapter(chatAdapter);
        rcvchat.setLayoutManager(new LinearLayoutManager(this, RecyclerView.VERTICAL, false));
    }

    private Emitter.Listener onNewMessageListener = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            ChatActivity.this.runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (args!=null){
                        String data = args[0].toString();
                        Message message = new Gson().fromJson(data, Message.class);
                        messages.add(message);
                        chatAdapter.notifyDataSetChanged();
                        if (!messages.isEmpty()){
                            rcvchat.smoothScrollToPosition(chatAdapter.getItemCount()-1);
                        }
                    }
                }
            });
        }
    };

    private void addListener(){
        getAllChat();
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        sendchat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String message = edtchat.getText().toString();
                if (message.isEmpty()){
                    showToast("Bạn chưa nhập tin nhắn");
                }
                else {
                    sendMessage(message);
                    edtchat.setText("");
                }
            }
        });

        mic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startIputSpeech();
            }
        });
    }

    private void startIputSpeech(){
        Intent intent = new Intent();
        intent.setAction(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.LANGUAGE_MODEL_FREE_FORM, "Say something...");

        startActivityForResult(intent, REQ_CODE_SPEECH_INPUT);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode==REQ_CODE_SPEECH_INPUT){
            if (resultCode==RESULT_OK && data!=null){
                ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                edtchat.setText(result.get(0));
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        socket.disconnect();
        socket.off("new_message", onNewMessageListener);
        socket.close();
    }

    private void sendMessage(String content) {
        socket.emit("create_messge", groupId, userInfo.getId(), content);
    }

    private void getAllChat(){
        retrofitService.getAllMessage(groupId).enqueue(new Callback<ArrayList<Message>>() {
            @Override
            public void onResponse(Call<ArrayList<Message>> call, Response<ArrayList<Message>> response) {
                ArrayList<Message> messageList = response.body();
                if (response.code()==200&&messageList!=null&&!messageList.isEmpty()){
                    messages.clear();
                    messages.addAll(messageList);
                    chatAdapter.notifyDataSetChanged();
                }
                else {

                }
            }

            @Override
            public void onFailure(Call<ArrayList<Message>> call, Throwable t) {

            }
        });
    }
}
