package com.nhuthao.codeme.View;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.nhuthao.codeme.Model.response.UserComment;
import com.nhuthao.codeme.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.ViewHolder> {

    private ArrayList<UserComment> commentList;

    public CommentAdapter(ArrayList<UserComment> commentList) {
        this.commentList = commentList;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_comment, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.BindView(commentList.get(position));
    }

    @Override
    public int getItemCount() {
        return commentList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.im_avatar_cmt)
        CircleImageView avatar;

        @BindView(R.id.tv_cmtname)
        TextView tv_name;

        @BindView(R.id.tv_cmt)
        TextView tv_cmt;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        private void BindView(UserComment userComment){
            Glide.with(itemView).load(userComment.getUserAvatar()).into(avatar);
            tv_name.setText(userComment.getFullName());
            tv_cmt.setText(userComment.getContent());
        }
    }
}
