package com.nhuthao.codeme.View.adapter;

import com.nhuthao.codeme.Model.response.Status;

public interface OnclickABC {
    void likeclick(int position);
    void commentclick(Status status);
    void shareclick(Status status);
    void avtarclick(Status status);
    void nameclick(Status status);
    void onCreatStatus(String content);
}
