package com.nhuthao.codeme.View.Home.tab_home;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.ViewFlipper;

import com.bumptech.glide.Glide;
import com.nhuthao.codeme.Model.request.CreatStatusSendForm;
import com.nhuthao.codeme.Model.request.LikeSendForm;
import com.nhuthao.codeme.Model.response.Status;
import com.nhuthao.codeme.Model.response.UserInfo;
import com.nhuthao.codeme.View.ProfileActivity;
import com.nhuthao.codeme.View.adapter.OnclickABC;
import com.nhuthao.codeme.R;
import com.nhuthao.codeme.View.CommentActivity;
import com.nhuthao.codeme.View.adapter.StatusAdapter;
import com.nhuthao.codeme.common.LoadingDialog;
import com.nhuthao.codeme.database.RealmContext;
import com.nhuthao.codeme.network.RetrofitService;
import com.nhuthao.codeme.network.RetrofitUtils;
import com.nhuthao.codeme.utils.Constant;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeFragment extends Fragment implements OnclickABC {


    public HomeFragment() {
        // Required empty public constructor
    }

    @BindView(R.id.view_flipper)
    ViewFlipper viewFlipper;

    @BindView(R.id.rcv_status)
    RecyclerView rcvstatus;


    private UserInfo userInfo;
    StatusAdapter statusAdapter;
    List<Status> statusList;
    Status currentStatus;
    private RetrofitService retrofitService;

    private LoadingDialog loadingDialog;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        init();
        addListener();
        getAllStatus();
    }

    private void init() {
        loadingDialog = new LoadingDialog(getContext());
        retrofitService = RetrofitUtils.getIntance().createService();
        userInfo = RealmContext.getInstance().getAll();
        statusList = new ArrayList<>();
        statusAdapter = new StatusAdapter(statusList, this);
        rcvstatus.setAdapter(statusAdapter);
        rcvstatus.setLayoutManager(new LinearLayoutManager(getContext(), rcvstatus.VERTICAL, false));
    }

    private void addListener(){

    }

    private void CreatStatus(String content){
        retrofitService.CreatStatus(new CreatStatusSendForm(userInfo.getId(), content)).enqueue(new Callback<Status>() {
            @Override
            public void onResponse(Call<Status> call, Response<Status> response) {
                Status status = response.body();
                if (response.code()==200&& status!=null){
                    statusList.add(0, status);
                    statusAdapter.notifyDataSetChanged();
                    showToast("Đăng bài thành công!");
                }
                else {
                    showToast("Đăng bài thất bại!");
                }
                loadingDialog.dismiss();
            }

            @Override
            public void onFailure(Call<Status> call, Throwable t) {
                loadingDialog.dismiss();
                showToast("Lỗi đường truyền, vui lòng kiểm tra lại kết nối Internet của bạn");
            }
        });
    }

    private void getAllStatus(){
        retrofitService.getAllStatus(userInfo.getId()).enqueue(new Callback<ArrayList<Status>>() {
            @Override
            public void onResponse(Call<ArrayList<Status>> call, Response<ArrayList<Status>> response) {
                ArrayList<Status> statuses = response.body();
                if (response.code()==200){
                        statusList.clear();
                        statusList.addAll(statuses);
                        statusAdapter.notifyDataSetChanged();
                        viewFlipper.setDisplayedChild(3);
                }
                else {
                        viewFlipper.setDisplayedChild(1);
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Status>> call, Throwable t) {
                viewFlipper.setDisplayedChild(2);
            }
        });
    }

    private void handleLikeAction(int position){
        statusList.get(position).setLike(!statusList.get(position).isLike());
        if (statusList.get(position).isLike()){
            statusList.get(position).setNumberLike(statusList.get(position).getNumberLike()+1);
        }
        else {
            statusList.get(position).setNumberLike(statusList.get(position).getNumberLike()-1);
        }
        statusAdapter.notifyItemChanged(position);
    }

    private void likeStatus(int position){

        retrofitService.LikeStatus(new LikeSendForm(userInfo.getId(), statusList.get(position)
                .getPostId())).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (response.code()==200){

                }
                else {
                    handleLikeAction(position);
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                handleLikeAction(position);
                showToast("Lỗi đường truyền, vui lòng kiểm tra lại kết nối Internet của bạn");
            }
        });
    }

    private void showToast(String message){
        Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
    }


    @Override
    public void likeclick(int position) {
        handleLikeAction(position);
        likeStatus(position);
    }

    @Override
    public void commentclick(Status status) {
        gotoComment(status);
    }

    @Override
    public void shareclick(Status status) {

    }

    @Override
    public void avtarclick(Status status) {
        gotoProfile(status);
    }

    @Override
    public void nameclick(Status status) {
        gotoProfile(status);
    }

    @Override
    public void onCreatStatus(String content) {
        loadingDialog.show();
        CreatStatus(content);
    }

    private void gotoComment1(Status status){
        Intent intent = new Intent(getContext(), CommentActivity.class);
        intent.putExtra("abc", status);
        startActivity(intent);
    }

    private void gotoComment(Status status){
        currentStatus = status;
        Intent intent = new Intent(getContext(), CommentActivity.class);
        intent.putExtra(Constant.POST_ID, status.getPostId());
        intent.putExtra(Constant.POST_IS_LIKE, status.isLike());
        intent.putExtra(Constant.POST_NUMBER_LIKE, status.getNumberLike());

        startActivityForResult(intent, Constant.REQUEST_CODE_COMMENT);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode== Activity.RESULT_OK && requestCode == Constant.REQUEST_CODE_COMMENT){
            boolean islike = data.getBooleanExtra(Constant.IS_LIKE, false);
            int numbercomment = data.getIntExtra(Constant.NUMBER_COMMENT, 0);
            int numberLike = data.getIntExtra(Constant.NUMBER_LIKE, 0);
            currentStatus.setLike(islike);
            currentStatus.setNumberLike(numberLike);
            currentStatus.setNumberComment(numbercomment);
            statusAdapter.notifyDataSetChanged();
        }
    }

    private void gotoProfile(Status status){
        Intent intent = new Intent(getContext(), ProfileActivity.class);
        intent.putExtra(ProfileActivity.KEY_USER, status.getAuthor());
        startActivity(intent);
    }
}
