package com.nhuthao.codeme.View.Home.tag_youtube;


import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.nhuthao.codeme.Model.response.UserInfo;
import com.nhuthao.codeme.R;
import com.nhuthao.codeme.database.RealmContext;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import io.realm.Realm;

/**
 * A simple {@link Fragment} subclass.
 */
public class YoutubeFragment extends Fragment {

    @BindView(R.id.youtuber)
    LinearLayout youtuber;

    @BindView(R.id.im_youtubeavatar)
    CircleImageView avatar;

    UserInfo userInfo;

    public YoutubeFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_youtube, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        init();
        addListener();
    }

    private void init(){
        userInfo = RealmContext.getInstance().getAll();
        Glide.with(this).load(userInfo.getAvatar()).into(avatar);
    }

    private void addListener(){
        youtuber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoChannel();
            }
        });
    }

    private void gotoChannel(){
        Intent intent = new Intent(this.getContext(), YoutubeChannelActivity.class);
        startActivity(intent);
    }
}
