package com.nhuthao.codeme.View.Chat;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.google.android.material.tabs.TabLayout;
import com.nhuthao.codeme.Model.request.CreatGroupChatSendForm;
import com.nhuthao.codeme.Model.response.GroupChatResponse;
import com.nhuthao.codeme.Model.response.UserInfo;
import com.nhuthao.codeme.R;
import com.nhuthao.codeme.View.Chat.Adapter.FriendListAdapter;
import com.nhuthao.codeme.View.Home.tab_chat.ChatFragment;
import com.nhuthao.codeme.base.BaseActivity;
import com.nhuthao.codeme.database.RealmContext;
import com.nhuthao.codeme.network.RetrofitService;
import com.nhuthao.codeme.network.RetrofitUtils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreateGroupActivity extends BaseActivity implements FriendListAdapter.OnFriendClickListener {

    @BindView(R.id.imv_back)
    ImageView back;

    @BindView(R.id.imv_check)
    ImageView check;

    @BindView(R.id.rcv_friendChat)
    RecyclerView rcvfriendchat;

    ArrayList<UserInfo> friendList;
    UserInfo userInfo;
    FriendListAdapter friendListAdapter;
    RetrofitService retrofitService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_group);
        ButterKnife.bind(this);
        init();
        getAllfriend();
        addListener();
    }

    private void init() {
        userInfo = RealmContext.getInstance().getAll();
        retrofitService = RetrofitUtils.getIntance().createService();
        friendList = new ArrayList<>();
        friendListAdapter = new FriendListAdapter(friendList, this);
        rcvfriendchat.setAdapter(friendListAdapter);
        rcvfriendchat.setLayoutManager(new LinearLayoutManager(this));
    }

    private void addListener(){
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        check.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CreatGroupChat();
            }
        });
    }

    private void getAllfriend(){
        retrofitService.getFriends(userInfo.getId()).enqueue(new Callback<ArrayList<UserInfo>>() {
            @Override
            public void onResponse(Call<ArrayList<UserInfo>> call, Response<ArrayList<UserInfo>> response) {
                ArrayList<UserInfo> friends = response.body();
                if (response.code()==200&&friends!=null){
                    friendList.clear();
                    friendList.addAll(friends);
                    friendListAdapter.notifyDataSetChanged();
                }
            }

            @Override
            public void onFailure(Call<ArrayList<UserInfo>> call, Throwable t) {

            }
        });
    }

    private void CreatGroupChat(){
        CreatGroupChatSendForm sendForm = new CreatGroupChatSendForm(getFriendSelected());
        retrofitService.CreatGroupChat(sendForm).enqueue(new Callback<GroupChatResponse>() {
            @Override
            public void onResponse(Call<GroupChatResponse> call, Response<GroupChatResponse> response) {
                GroupChatResponse groupChatResponse = response.body();
                if (response.code()==200 && groupChatResponse != null){
                    showToast("Khởi tạo thành công");
                    gotoChat(groupChatResponse.get_id());
                    finish();
                }
                else {
                    showToast("Khởi tạo thất bại");
                }
            }

            @Override
            public void onFailure(Call<GroupChatResponse> call, Throwable t) {
                showToast("Khởi tạo thất bại");
            }
        });
    }

    private ArrayList<String> getFriendSelected(){
        ArrayList<String> list = new ArrayList<>();
        list.add(userInfo.getId());
        for (UserInfo friend: friendList){
            if (friend.isSelected()){
                list.add(friend.getId());
            }
        }
        return list;
    }

    private void gotoChat(String groupId){
        Intent intent = new Intent(this, ChatActivity.class);
        intent.putExtra(ChatActivity.GROUP_ID, groupId);
        startActivity(intent);
    }

    @Override
    public void onFriendClick() {
        boolean visible = false;
        for (UserInfo friend: friendList){
            if (friend.isSelected()){
                visible = true;
                break;
            }
        }
        if (visible){
            check.setVisibility(View.VISIBLE);
        }
        else {
            check.setVisibility(View.GONE);
        }
    }
}
