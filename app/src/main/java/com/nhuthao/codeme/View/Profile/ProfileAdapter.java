package com.nhuthao.codeme.View.Profile;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.nhuthao.codeme.Model.response.Status;
import com.nhuthao.codeme.Model.response.UserInfo;
import com.nhuthao.codeme.R;
import com.nhuthao.codeme.database.RealmContext;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class ProfileAdapter extends RecyclerView.Adapter<ProfileAdapter.ViewHolder> {

    interface click{
        void likeclick(int position);
        void commentclick(Status status);
        void avtarclick(Status status);
        void nameclick(Status status);
        void onCreatStatus(String content);
    }


    ArrayList<Status> list;
    UserInfo userInfo = RealmContext.getInstance().getAll();

    public ProfileAdapter(ArrayList<Status> list) {
        this.list = list;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.iteam_status, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bindView(list.get(position), position);
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.iv_circle_item)
        CircleImageView imvavatar;

        @BindView(R.id.tv_name_item)
        TextView tvname;

        @BindView(R.id.tv_date)
        TextView tvdate;

        @BindView(R.id.tv_status)
        TextView tvstatus;

        @BindView(R.id.tv_number_like)
        TextView tvnumberlike;

        @BindView(R.id.tv_number_cmt)
        TextView tvnumbercmt;

        @BindView(R.id.like)
        LinearLayout like;

        @BindView(R.id.comment)
        LinearLayout comment;

        @BindView(R.id.share)
        LinearLayout share;

        @BindView(R.id.im_like)
        ImageView imlike;

        @BindView(R.id.tv_like)
        TextView tvlike;

        Status status;

        int position;
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        private void Addlistnener(){

        }

        private void bindView(Status status, int position){
            this.status=status;
            this.position= position;
            Glide.with(itemView.getContext()).load(userInfo.getAvatar()).into(imvavatar);
            tvname.setText(status.getAuthorName());
            tvdate.setText(status.getCreateDate());
            tvnumberlike.setText(Integer.toString(status.getNumberLike()));
            tvnumbercmt.setText(Integer.toString(status.getNumberComment()));
            tvstatus.setText(status.getContent());
            if (status.isLike()){
                Glide.with(imlike.getContext()).load(R.drawable.ic_like).into(imlike);
                tvlike.setTextColor(itemView.getResources().getColor(R.color.colorRed500));
            }
            else {
                imlike.setBackground(imlike.getResources().getDrawable(R.drawable.ic_dont_like));
                tvlike.setTextColor(itemView.getResources().getColor(R.color.colorGrey700));
            }
        }
    }
}
