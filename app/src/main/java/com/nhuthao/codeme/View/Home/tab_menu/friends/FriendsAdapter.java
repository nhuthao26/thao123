package com.nhuthao.codeme.View.Home.tab_menu.friends;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.nhuthao.codeme.Model.response.UserInfo;
import com.nhuthao.codeme.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class FriendsAdapter extends RecyclerView.Adapter<FriendsAdapter.ViewHolder> {

    private ArrayList<UserInfo> userInfoList;

    public FriendsAdapter(ArrayList<UserInfo> userInfoList) {
        this.userInfoList = userInfoList;
    }



    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.imv_avatarfriends)
        CircleImageView avatarfriends;

        @BindView(R.id.txt_friends)
        TextView txtfriends;


        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        private void bindView(UserInfo userInfo){
            Glide.with(itemView.getContext()).load(userInfo.getAvatar()).into(avatarfriends);
            txtfriends.setText(userInfo.getFullName());
        }
    }
    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_friends, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bindView(userInfoList.get(position));
    }

    @Override
    public int getItemCount() {
        return userInfoList.size();
    }
}
