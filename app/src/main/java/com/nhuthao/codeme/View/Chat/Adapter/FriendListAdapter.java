package com.nhuthao.codeme.View.Chat.Adapter;

import android.media.Image;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.nhuthao.codeme.Model.response.UserInfo;
import com.nhuthao.codeme.R;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class FriendListAdapter extends RecyclerView.Adapter<FriendListAdapter.ViewHolder> {

    public interface OnFriendClickListener{
        void onFriendClick();
    }

    private ArrayList<UserInfo> userInfoList;
    private OnFriendClickListener listener;

    public FriendListAdapter(ArrayList<UserInfo> userInfoList, OnFriendClickListener listener) {
        this.userInfoList = userInfoList;
        this.listener = listener;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_friendchat)
        TextView tvfriend;

        @BindView(R.id.imv_avatarfriendchat)
        CircleImageView avatarfriend;

        @BindView(R.id.imv_checkk)
        ImageView imvcheck;

        @BindView(R.id.imv_add)
        ImageView imvadd;

        private UserInfo friend;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    friend.setSelected(!friend.isSelected());
                    notifyDataSetChanged();
                    listener.onFriendClick();
                }
            });
        }

        private void bindView(UserInfo friend){
            this.friend = friend;
            Glide.with(itemView.getContext()).load(friend.getAvatar()).into(avatarfriend);
            tvfriend.setText(friend.getFullName());

            if (friend.isSelected()){
                imvcheck.setVisibility(View.VISIBLE);
                imvadd.setVisibility(View.GONE);
            }
            else {
                imvcheck.setVisibility(View.GONE);
                imvadd.setVisibility(View.VISIBLE);
            }
        }
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_friendchat, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bindView(userInfoList.get(position));
    }


    @Override
    public int getItemCount() {
        return userInfoList.size();
    }
}
