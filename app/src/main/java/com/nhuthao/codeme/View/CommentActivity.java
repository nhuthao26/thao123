package com.nhuthao.codeme.View;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.nhuthao.codeme.Model.request.CommentSendForm;
import com.nhuthao.codeme.Model.request.LikeSendForm;
import com.nhuthao.codeme.Model.response.Status;
import com.nhuthao.codeme.Model.response.UserComment;
import com.nhuthao.codeme.Model.response.UserInfo;
import com.nhuthao.codeme.R;
import com.nhuthao.codeme.database.RealmContext;
import com.nhuthao.codeme.network.RetrofitService;
import com.nhuthao.codeme.network.RetrofitUtils;
import com.nhuthao.codeme.utils.Constant;


import java.util.ArrayList;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CommentActivity extends AppCompatActivity {

    public static final int REQUEST_CODE_SPEECH = 1;

    @BindView(R.id.imv_like)
    ImageView imvlike;

    @BindView(R.id.txt_number_cmt)
    TextView txtnumberlike;

    @BindView(R.id.edt_cmt)
    EditText edtcmt;

    @BindView(R.id.imv_cmt)
    ImageView imvcomment;

    @BindView(R.id.rcv_comment)
    RecyclerView rcvcomment;

    @BindView(R.id.imv_mic_cmt)
    ImageView miccomment;


//    Status status;
    RetrofitService retrofitService;
    UserInfo userInfo;
    ArrayList<UserComment> userCommentList;
    CommentAdapter commentAdapter;

    String statusId;
    int numberLike;
    boolean isLike;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment);
        ButterKnife.bind(this);
        init();
        addListener();
    }

    @SuppressLint("SetTextI18n")
    private void init(){
        retrofitService = RetrofitUtils.getIntance().createService();
//        status = (Status) getIntent().getSerializableExtra("abc");


        Intent intent = getIntent();
        statusId = intent.getStringExtra(Constant.POST_ID);
        numberLike = intent.getIntExtra(Constant.POST_NUMBER_LIKE, 0);
        isLike = intent.getBooleanExtra(Constant.POST_IS_LIKE, false);

        txtnumberlike.setText(Integer.toString(numberLike));
        userInfo= RealmContext.getInstance().getAll();
        userCommentList = new ArrayList<>();
        commentAdapter = new CommentAdapter(userCommentList);
        rcvcomment.setAdapter(commentAdapter);
        rcvcomment.setLayoutManager(new LinearLayoutManager(this,
                LinearLayoutManager.VERTICAL, false));
    }

    private void addListener() {
        liketrueStatus();
        getComment();

        imvcomment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String comment = edtcmt.getText().toString();
                if (comment.isEmpty()){
                    showToast("Bạn vui lòng điền nội dung");
                }
                else {
                    Comment(comment);
                }
            }
        });

        imvlike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isLike){
                    imvlike.setBackground(imvlike.getResources().getDrawable(R.drawable.ic_dont_like));
                }
                else {
                    imvlike.setBackground(imvlike.getResources().getDrawable(R.drawable.ic_like));
                }
                handleLikeAction();
                likeStatus();
            }
        });

        miccomment.setOnClickListener(v -> startInputSpeech());
    }

    private void startInputSpeech(){
        Intent intent = new Intent();
        intent.setAction(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL, RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, Locale.getDefault());
        intent.putExtra(RecognizerIntent.LANGUAGE_MODEL_FREE_FORM, "Say something...");

        startActivityForResult(intent, REQUEST_CODE_SPEECH);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_CODE_SPEECH){
            if (resultCode == RESULT_OK && data != null){
                ArrayList<String> result = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
                edtcmt.setText(result.get(0));
            }
        }
    }

    private void getComment(){
        retrofitService.getAllComment(statusId).enqueue(new Callback<ArrayList<UserComment>>() {
            @Override
            public void onResponse(Call<ArrayList<UserComment>> call, Response<ArrayList<UserComment>> response) {
                ArrayList<UserComment> userComments = response.body();
                if (response.code()==200&&userComments!=null){
                    userCommentList.clear();
                    userCommentList.addAll(userComments);
                    commentAdapter.notifyDataSetChanged();
                }
                else {

                }
            }

            @Override
            public void onFailure(Call<ArrayList<UserComment>> call, Throwable t) {

            }
        });
    }

    private void Comment(String content){
        retrofitService.postComment(new CommentSendForm(userInfo.getId(),
                statusId, content)).enqueue(new Callback<UserComment>() {
            @Override
            public void onResponse(Call<UserComment> call, Response<UserComment> response) {
                UserComment userComment = response.body();
                if (response.code()==200&&userComment!=null){
                    userCommentList.add(userComment);
                    commentAdapter.notifyDataSetChanged();
                    edtcmt.setText("");
                }
                else {
                    showToast("Kết nối mạng không ổn định");
                }
            }

            @Override
            public void onFailure(Call<UserComment> call, Throwable t) {
                showToast("Lỗi kết nối, vui lòng thử lại!");
            }
        });
    }

    private void likeStatus(){
        retrofitService.LikeStatus(new LikeSendForm(userInfo.getId(), statusId)).enqueue(new Callback<Void>() {
            @Override
            public void onResponse(Call<Void> call, Response<Void> response) {
                if (response.code()==200){

                }
                else {
                    handleLikeAction();
                    liketrueStatus();
                }
            }

            @Override
            public void onFailure(Call<Void> call, Throwable t) {
                handleLikeAction();
                liketrueStatus();
                showToast("Lỗi đường truyền, vui lòng kiểm tra lại kết nối Internet của bạn");
            }
        });
    }

    private void handleLikeAction(){
        isLike = !isLike;
        if (isLike){
//            status.setNumberLike(status.getNumberLike()+1);
            numberLike = numberLike +1;
        }
        else {
//            status.setNumberLike(status.getNumberLike()-1);
            numberLike = numberLike - 1;
        }
        txtnumberlike.setText(Integer.toString(numberLike));
    }

    @Override
    public void onBackPressed() {
        Intent intent = new Intent();
        intent.putExtra(Constant.IS_LIKE, isLike);
        intent.putExtra(Constant.NUMBER_LIKE, numberLike);
        intent.putExtra(Constant.NUMBER_COMMENT, userCommentList.size());
        setResult(Activity.RESULT_OK, intent);
        super.onBackPressed();
    }

    private void liketrueStatus(){
        if (isLike){
            imvlike.setBackground(imvlike.getResources().getDrawable(R.drawable.ic_like));
        }
        else {
            imvlike.setBackground(imvlike.getResources().getDrawable(R.drawable.ic_dont_like));
        }
    }

    private void showToast(String message){
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}
