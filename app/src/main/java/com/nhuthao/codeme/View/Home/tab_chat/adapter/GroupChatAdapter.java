package com.nhuthao.codeme.View.Home.tab_chat.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ViewFlipper;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.nhuthao.codeme.Model.response.Groupchat;
import com.nhuthao.codeme.Model.response.UserInfo;
import com.nhuthao.codeme.R;
import com.nhuthao.codeme.database.RealmContext;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class GroupChatAdapter extends RecyclerView.Adapter<GroupChatAdapter.ViewHolder> {

    public interface OnGroupChatClick{
        void onGroupChatClick(String groupId);
    }

    private OnGroupChatClick listener;
    private ArrayList<Groupchat> groupchatList;

    public GroupChatAdapter(ArrayList<Groupchat> groupchatList, OnGroupChatClick listener) {
        this.groupchatList = groupchatList;
        this.listener = listener;
    }

    UserInfo userInfo = RealmContext.getInstance().getAll();
    ArrayList<UserInfo> users;

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_group_chat, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.BindView(groupchatList.get(position));
    }

    @Override
    public int getItemCount() {
        return groupchatList.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.linear_chat)
        LinearLayout linearchat;

        @BindView(R.id.imv_avatar1)
        CircleImageView imvavatar1;

        @BindView(R.id.imv_avatar2)
        CircleImageView imvavatar2;

        @BindView(R.id.imv_avatar3)
        CircleImageView imvavatar3;

        @BindView(R.id.txt_groupname)
        TextView groupname;

        @BindView(R.id.txt_lastmessage)
        TextView lastmessage;

        @BindView(R.id.view_flipperChat)
        ViewFlipper viewFlipper;

        private Groupchat groupchat;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            init();
        }

        private void init(){
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.onGroupChatClick(groupchat.getGroupId());
                }
            });
        }

        private void BindView(Groupchat groupchat){
            this.groupchat = groupchat;
            ArrayList<UserInfo> users = groupchat.getUsers();
            String username1 = "";
            String username2 = "";

            if (users.size()>2){
                viewFlipper.setDisplayedChild(1);
                int count=1;
                for (UserInfo user: users){
                    if (!user.getId().equals(userInfo.getId())){
                        if(count==1){
                            Glide.with(itemView).load(user.getAvatar()).into(imvavatar2);
                            String[] name1 = user.getFullName().split(" ");
                            username1 = name1[name1.length-1];
                            count++;
                        }
                        else {
                            Glide.with(itemView).load(user.getAvatar()).into(imvavatar3);
                            String[] name2= user.getFullName().split(" ");
                            username2 = name2[name2.length-1];
                            break;
                        }
                    }
                }
                if (groupchat.getGroupName().isEmpty()){
                    groupname.setText(username1+", "+username2+",...");
                }
                else {
                    groupname.setText(groupchat.getGroupName());
                }
            }
            else {
                viewFlipper.setDisplayedChild(0);
                for (UserInfo user: users){
                    if (!user.getId().equals(userInfo.getId())){
                        Glide.with(itemView).load(user.getAvatar()).into(imvavatar1);
                        if (groupchat.getGroupName().isEmpty()){
                            groupname.setText(user.getFullName());
                        }
                        else {
                            groupname.setText(groupchat.getGroupName());
                        }
                        break;
                    }
                }

            }
            lastmessage.setText(groupchat.getLastMessage());
        }
    }

}
