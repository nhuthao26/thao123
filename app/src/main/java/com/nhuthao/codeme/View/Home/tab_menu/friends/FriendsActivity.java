package com.nhuthao.codeme.View.Home.tab_menu.friends;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;

import com.bumptech.glide.Glide;
import com.nhuthao.codeme.Model.response.UserInfo;
import com.nhuthao.codeme.R;
import com.nhuthao.codeme.database.RealmContext;
import com.nhuthao.codeme.network.RetrofitService;
import com.nhuthao.codeme.network.RetrofitUtils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FriendsActivity extends AppCompatActivity {

    @BindView(R.id.rcv_friends)
    RecyclerView rcvfriends;

    @BindView(R.id.im_avatar_profilefriends)
    CircleImageView avatar;

    UserInfo userInfo;
    ArrayList<UserInfo> friendList;
    FriendsAdapter friendsAdapter;
    RetrofitService retrofitService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friends);
        ButterKnife.bind(this);
        init();
        addListener();
    }

    private void init(){
        userInfo = RealmContext.getInstance().getAll();
        retrofitService = RetrofitUtils.getIntance().createService();
        friendList=new ArrayList<>();
        friendsAdapter = new FriendsAdapter(friendList);
        rcvfriends.setAdapter(friendsAdapter);
        rcvfriends.setLayoutManager(new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false));
        Glide.with(this).load(userInfo.getAvatar()).into(avatar);
    }

    private void addListener(){
        retrofitService.getFriends(userInfo.getId()).enqueue(new Callback<ArrayList<UserInfo>>() {
            @Override
            public void onResponse(Call<ArrayList<UserInfo>> call, Response<ArrayList<UserInfo>> response) {
                ArrayList<UserInfo> friends = response.body();
                if (response.code()==200&&friendList!=null){
                    friendList.clear();
                    friendList.addAll(friends);
                    friendsAdapter.notifyDataSetChanged();
                }
                else {

                }
            }

            @Override
            public void onFailure(Call<ArrayList<UserInfo>> call, Throwable t) {

            }
        });
    }
}
