package com.nhuthao.codeme.View.Chat.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.nhuthao.codeme.Model.response.Message;
import com.nhuthao.codeme.Model.response.UserInfo;
import com.nhuthao.codeme.R;
import com.nhuthao.codeme.database.RealmContext;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class ChatAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TYPE_MY_MESSAGE = 0;
    private static final int TYPE_FRIEND_MESSAGE = 1;
    private ArrayList<Message> messageList;
    private UserInfo userInfo = RealmContext.getInstance().getAll();

    public ChatAdapter(ArrayList<Message> messageList) {
        this.messageList = messageList;
    }

    class ViewHoderMyMessage extends RecyclerView.ViewHolder {

        @BindView(R.id.tv_my_chat)
        TextView tvmychat;

        public ViewHoderMyMessage(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        private void bindMyView(Message message) {
            tvmychat.setText(message.getContent());
        }

    }

    class ViewHoderFriendMessage extends RecyclerView.ViewHolder {

        @BindView(R.id.im_avatar_chat)
        CircleImageView avatar;

        @BindView(R.id.tv_friend_chat)
        TextView tvfriendchat;

        @BindView(R.id.tv_name_friend)
        TextView tvname;

        public ViewHoderFriendMessage(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        private void bindFriendView(Message message) {
            Glide.with(itemView.getContext()).load(message.getAvatar()).into(avatar);
            tvfriendchat.setText(message.getContent());
            String[] name = message.getFullName().split(" ");
            tvname.setText(name[name.length - 1]);
        }

    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == TYPE_MY_MESSAGE) {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_my_message, parent, false);
            return new ViewHoderMyMessage(view);
        } else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_friend_message, parent, false);
            return new ViewHoderFriendMessage(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof ViewHoderMyMessage) {
            ((ViewHoderMyMessage) holder).bindMyView(messageList.get(position));
        } else if (holder instanceof ViewHoderFriendMessage) {
            ((ViewHoderFriendMessage) holder).bindFriendView(messageList.get(position));
        }
    }

    @Override
    public int getItemCount() {
        return messageList.size();
    }

    @Override
    public int getItemViewType(int position) {
        Message message = messageList.get(position);
        if (message.getId().equals(userInfo.getId())) {
            return TYPE_MY_MESSAGE;
        } else {
            return TYPE_FRIEND_MESSAGE;
        }
    }
}
