package com.nhuthao.codeme.View.Home.Adapter;


import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;

import com.nhuthao.codeme.View.Home.tab_chat.ChatFragment;
import com.nhuthao.codeme.View.Home.tab_home.HomeFragment;
import com.nhuthao.codeme.View.Home.tab_menu.MenuFragment;
import com.nhuthao.codeme.View.Home.tag_youtube.YoutubeFragment;

public class ViewPagerAdapter extends FragmentPagerAdapter {

    public ViewPagerAdapter(@NonNull FragmentManager fm) {
        super(fm);
    }

    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {

        return null;
    }

    @NonNull
    @Override
    public Fragment getItem(int position) {

        switch (position){
            case 0:
                return new HomeFragment();
            case 1:
                return new YoutubeFragment();
            case 2:
                return new ChatFragment();
            case 3:
                return new MenuFragment();
        }
        return null;
    }

    @Override
    public int getCount() {
        return 4;
    }
}
