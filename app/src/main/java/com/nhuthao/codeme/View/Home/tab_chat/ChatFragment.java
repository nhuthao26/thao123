package com.nhuthao.codeme.View.Home.tab_chat;


import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ViewFlipper;

import com.bumptech.glide.Glide;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.nhuthao.codeme.Model.response.Groupchat;
import com.nhuthao.codeme.Model.response.UserInfo;
import com.nhuthao.codeme.R;
import com.nhuthao.codeme.View.Chat.ChatActivity;
import com.nhuthao.codeme.View.Chat.CreateGroupActivity;
import com.nhuthao.codeme.View.Home.tab_chat.adapter.GroupChatAdapter;
import com.nhuthao.codeme.base.BaseFragment;
import com.nhuthao.codeme.database.RealmContext;
import com.nhuthao.codeme.network.RetrofitService;
import com.nhuthao.codeme.network.RetrofitUtils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static androidx.constraintlayout.motion.widget.MotionScene.TAG;

/**
 * A simple {@link Fragment} subclass.
 */
public class ChatFragment extends BaseFragment implements GroupChatAdapter.OnGroupChatClick {

    @BindView(R.id.rcv_groupchat)
    RecyclerView rcvgroupchat;

    @BindView(R.id.float_add)
    FloatingActionButton floatadd;

    @BindView(R.id.im_avatarchatchat)
    CircleImageView avatar;

    @BindView(R.id.view_flipperChat)
    ViewFlipper flipper;

    RetrofitService retrofitService;
    UserInfo userInfo;
    ArrayList<Groupchat> groupchatArrayList;
    GroupChatAdapter groupChatAdapter;

    public ChatFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_chat, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        init();
        addListener();
        getMychat();
    }

    private void init() {
        userInfo= RealmContext.getInstance().getAll();
        Glide.with(this).load(userInfo.getAvatar()).into(avatar);
        retrofitService = RetrofitUtils.getIntance().createService();
        groupchatArrayList = new ArrayList<>();
        groupChatAdapter = new GroupChatAdapter(groupchatArrayList, this);
        rcvgroupchat.setAdapter(groupChatAdapter);
        rcvgroupchat.setLayoutManager(new LinearLayoutManager(getContext()));

    }

    private void addListener(){
        floatadd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoCreateGroupchat();
            }
        });
    }

    private void getMychat(){

        retrofitService.getMychat(userInfo.getId()).enqueue(new Callback<ArrayList<Groupchat>>() {
            @Override
            public void onResponse(Call<ArrayList<Groupchat>> call, Response<ArrayList<Groupchat>> response) {
                ArrayList<Groupchat> groupchats = response.body();
                if (response.code()==200&&groupchats!=null){
                    groupchatArrayList.clear();
                    groupchatArrayList.addAll(groupchats);
                    groupChatAdapter.notifyDataSetChanged();
                    flipper.setDisplayedChild(3);
                }
                else {
                    flipper.setDisplayedChild(1);
                }
            }

            @Override
            public void onFailure(Call<ArrayList<Groupchat>> call, Throwable t) {
                flipper.setDisplayedChild(2);
            }
        });
    }

    private void gotoCreateGroupchat(){
        Intent intent = new Intent(this.getContext(), CreateGroupActivity.class);
        startActivity(intent);
    }

    @Override
    public void onGroupChatClick(String groupId) {
        gotoChat(groupId);
    }

    private void gotoChat(String groupId){
        Intent intent = new Intent(this.getContext(), ChatActivity.class);
        intent.putExtra(ChatActivity.GROUP_ID, groupId);
        startActivity(intent);
    }
}
