package com.nhuthao.codeme.View.Home.tab_menu;


import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.nhuthao.codeme.Model.response.UserInfo;
import com.nhuthao.codeme.R;
import com.nhuthao.codeme.View.Home.tab_menu.friends.FriendsActivity;
import com.nhuthao.codeme.View.ProfileActivity;
import com.nhuthao.codeme.View.authen.AuthenActivity;
import com.nhuthao.codeme.database.RealmContext;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

/**
 * A simple {@link Fragment} subclass.
 */
public class MenuFragment extends Fragment {

    @BindView(R.id.linear_signout)
    LinearLayout linearsignout;

    @BindView(R.id.linear_friends)
    LinearLayout friends;

    @BindView(R.id.iv_circle)
    CircleImageView circleImageView;

    @BindView(R.id.tv_name)
    TextView tvname;

    @BindView(R.id.imvfriends)
    ImageView imvfriends;

    @BindView(R.id.imv_profile)
    ImageView imvprofile;

    @BindView(R.id.imv_signout)
    ImageView imvsignout;

    @BindView(R.id.linear_profile)
    LinearLayout profile;

    @BindView(R.id.linear_profile1)
    LinearLayout profile1;

    UserInfo userInfo;

    public MenuFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_menu, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ButterKnife.bind(this, view);
        userInfo = RealmContext.getInstance().getAll();
        Glide.with(getContext()).load(userInfo.getAvatar()).into(circleImageView);
        tvname.setText(userInfo.getFullName());
        addListner();
    }

    private void addListner(){
        imvfriends.setColorFilter(Color.parseColor("#8e8e8e"), PorterDuff.Mode.SRC_IN);
        imvprofile.setColorFilter(Color.parseColor("#8e8e8e"), PorterDuff.Mode.SRC_IN);
        imvsignout.setColorFilter(Color.parseColor("#8e8e8e"), PorterDuff.Mode.SRC_IN);

        linearsignout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signout();
            }
        });

        profile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoProfile(userInfo.getUsername());
            }
        });

        profile1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoProfile(userInfo.getUsername());
            }
        });

        friends.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoFriends();
            }
        });
    }

    private void signout(){
        new AlertDialog.Builder(getActivity()).setIcon(R.drawable.help).setTitle("Xác nhận đăng xuất")
                .setMessage("Bạn có chắc chắn muốn đăng xuất không?")
                .setNegativeButton("Đăng xuất", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        UserInfo userInfo = RealmContext.getInstance().getAll();
                        RealmContext.getInstance().delete(userInfo);
                        dangxuat();
                    }
                })
                .setPositiveButton("Hủy", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                })
                .show();
    }

    private void dangxuat(){
        Intent intent = new Intent(getActivity(), AuthenActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    private void gotoProfile(String Username){
        Intent intent = new Intent(getActivity(), ProfileActivity.class);
        intent.putExtra(ProfileActivity.KEY_USER, Username);
        startActivity(intent);
    }

    private void gotoFriends(){
        Intent intent = new Intent(getActivity(), FriendsActivity.class);
        startActivity(intent);
    }
}
