package com.nhuthao.codeme.View.authen;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import com.nhuthao.codeme.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class AuthenActivity extends AppCompatActivity {

    @BindView(R.id.bt_dangnhap1)
    Button btdangnhap;

    @BindView(R.id.bt_dangkyGG)
    Button btdangkyGG;

    @BindView(R.id.linear_dangky1)
    LinearLayout linnear_dangky;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_authen);

        ButterKnife.bind(this);


        addListener();
    }

    private void addListener() {

        btdangnhap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoLogin();
            }
        });

        linnear_dangky.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoAdd();
            }
        });
    }

    private void gotoLogin(){
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }

    private void gotoAdd(){
        Intent intent = new Intent(this, AddActivity.class);
        startActivity(intent);
    }

}
