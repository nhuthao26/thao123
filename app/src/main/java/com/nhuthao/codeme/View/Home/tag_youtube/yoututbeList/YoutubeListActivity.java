package com.nhuthao.codeme.View.Home.tag_youtube.yoututbeList;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.nhuthao.codeme.Model.response.YoutubeResponce.Channel;
import com.nhuthao.codeme.Model.response.YoutubeResponce.Item;
import com.nhuthao.codeme.R;
import com.nhuthao.codeme.View.Home.tag_youtube.YoutubeVideoActivity;
import com.nhuthao.codeme.base.BaseActivity;
import com.nhuthao.codeme.network.Youtube.RetrofitYoutube;
import com.nhuthao.codeme.network.Youtube.YoutubeRetrofitService;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class YoutubeListActivity extends BaseActivity {

    final String PART_KEY = "contentDetails%2C%20id%2C%20snippet%2C%20status";
    final String PLAY_LIST_ID = "PLBcAa442MLArA5s6qWibG6TpEonVQp11b";
    final String KEY = "AIzaSyBZvkmjFExwjcl_WbNhmhXihgMzugEZGDw";

    @BindView(R.id.imv_youtube_video)
    ImageView imvyoutubevideo;

    @BindView(R.id.list_youtube_video)
    LinearLayout listyoutubevideo;

    YoutubeRetrofitService retrofitService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_youtube_list);
        ButterKnife.bind(this);
        init();
        addListenner();
    }

    private void init(){
        retrofitService = RetrofitYoutube.getIntance().createService();
        retrofitService.GetAllVideo(PART_KEY,PLAY_LIST_ID, KEY).enqueue(new Callback<Channel>() {
            @Override
            public void onResponse(Call<Channel> call, Response<Channel> response) {
                Channel channel = response.body();
                if (response.code()==200 && channel!=null){
                    Log.d("nhuthao123", channel.toString());
                }
                else {
                    Log.d("nhuthao123", "Get fail");
                }
            }

            @Override
            public void onFailure(Call<Channel> call, Throwable t) {
                Log.d("nhuthao123", "Get fail 2");
            }
        });

    }

    private void addListenner(){
        imvyoutubevideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoVideo();
            }
        });

        listyoutubevideo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoVideo();
            }
        });
    }

        private void gotoVideo(){
        Intent intent = new Intent(this, YoutubeVideoActivity.class);
        startActivity(intent);
    }
}
