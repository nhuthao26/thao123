package com.nhuthao.codeme.View.Home;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.Bundle;

import com.google.android.material.tabs.TabLayout;
import com.nhuthao.codeme.Model.response.UserInfo;
import com.nhuthao.codeme.R;
import com.nhuthao.codeme.View.authen.AuthenActivity;
import com.nhuthao.codeme.View.Home.Adapter.ViewPagerAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeActivity extends AppCompatActivity {

    @BindView(R.id.view_pager)
    ViewPager viewPager;

    @BindView(R.id.tab_layout)
    TabLayout tabLayout;

    ViewPagerAdapter adapter;

    UserInfo userInfo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        ButterKnife.bind(this);
        init();
        addListener();
    }

    private void init(){
        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        viewPager.setAdapter(adapter);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.getTabAt(0).setIcon(R.drawable.news_feeds);
        tabLayout.getTabAt(0).getIcon()
                .setColorFilter(Color.BLUE, PorterDuff.Mode.SRC_IN);

        tabLayout.getTabAt(1).setIcon(R.drawable.ic_youtube);
        tabLayout.getTabAt(1).getIcon()
                .setColorFilter(Color.parseColor("#8e8e8e"), PorterDuff.Mode.SRC_IN);

        tabLayout.getTabAt(2).setIcon(R.drawable.ic_chat);
        tabLayout.getTabAt(2).getIcon()
                .setColorFilter(Color.parseColor("#8e8e8e"), PorterDuff.Mode.SRC_IN);

        tabLayout.getTabAt(3).setIcon(R.drawable.ic_menu);
        tabLayout.getTabAt(3).getIcon()
                .setColorFilter(Color.parseColor("#8e8e8e"), PorterDuff.Mode.SRC_IN);
    }

    private void addListener() {
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                tab.getIcon().setColorFilter(Color.BLUE, PorterDuff.Mode.SRC_IN);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                tab.getIcon().setColorFilter(Color.parseColor("#8e8e8e"), PorterDuff.Mode.SRC_IN);
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private void gotoMain(){
        Intent intent = new Intent(this, AuthenActivity.class);
        startActivity(intent);
        finish();
    }
}
