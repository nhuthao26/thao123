package com.nhuthao.codeme.View;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ViewFlipper;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.glide.slider.library.SliderLayout;
import com.glide.slider.library.SliderTypes.DefaultSliderView;
import com.google.android.gms.tasks.Continuation;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.nhuthao.codeme.Model.response.AvatarUrl;
import com.nhuthao.codeme.Model.response.Profile;
import com.nhuthao.codeme.Model.response.Status;
import com.nhuthao.codeme.Model.response.UserInfo;
import com.nhuthao.codeme.R;
import com.nhuthao.codeme.View.adapter.OnclickABC;
import com.nhuthao.codeme.View.adapter.StatusAdapter;
import com.nhuthao.codeme.base.BaseActivity;
import com.nhuthao.codeme.database.RealmContext;
import com.nhuthao.codeme.network.RetrofitService;
import com.nhuthao.codeme.network.RetrofitUtils;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfileActivity extends BaseActivity implements OnclickABC {

    final public static String KEY_USER = "abc_xyz";
    final public static int REQUEST_PERMISSIONS_CODE = 1;
    final public static int REQUEST_GET_IMAGE_CODE = 2;
    final public static int REQUEST_CALL_PHONE_CODE = 3;

    @BindView(R.id.slider_layout)
    SliderLayout sliderLayout;

    @BindView(R.id.camera)
    CircleImageView camera;

    @BindView(R.id.imv_bigavatar)
    CircleImageView imv_bigavatar;


    @BindView(R.id.txt_name)
    TextView name;

    @BindView(R.id.tv_andress)
    TextView andress;

    @BindView(R.id.tv_phone)
    TextView phone;

    @BindView(R.id.tv_birthday)
    TextView birthday;

    @BindView(R.id.rv_status)
    RecyclerView rcvstatus;

    @BindView(R.id.view_flipper1)
    ViewFlipper viewFlipper1;

    @BindView(R.id.view_phone)
    LinearLayout viewphone;

    @BindView(R.id.view_andress)
    LinearLayout viewandress;

    @BindView(R.id.view_birthday)
    LinearLayout viewbirthday;

    UserInfo userInfo;
    RetrofitService retrofitService;
    String username;
    Profile profile;
    ArrayList<Status> statusList;
    StatusAdapter statusAdapter;
    String[] listPermissions = null;
    StorageReference reference;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        ButterKnife.bind(this);
        init();
        addListener();
    }

    private void init() {
        retrofitService = RetrofitUtils.getIntance().createService();
        userInfo = RealmContext.getInstance().getAll();
        username = (String) getIntent().getSerializableExtra(KEY_USER);
        reference = FirebaseStorage.getInstance().getReference();
//        if (username.equals(userInfo.getUsername())) {
//            camera.setVisibility(View.VISIBLE);
//            poststatus.setVisibility(View.VISIBLE);
//        } else {
//            camera.setVisibility(View.GONE);
//            poststatus.setVisibility(View.GONE);
//        }
        statusList = new ArrayList<>();
        statusAdapter = new StatusAdapter(statusList, this);
        rcvstatus.setAdapter(statusAdapter);
        rcvstatus.setLayoutManager(new LinearLayoutManager(
                getApplicationContext(), rcvstatus.VERTICAL, false));
    }

    private void addListener() {
        getProfile();
        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ensurePermissions();
            }
        });

        viewphone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listPermissions = new String[]{Manifest.permission.CALL_PHONE};
                if (checkPermissions(ProfileActivity.this, listPermissions)){
                    callphone(profile.getPhone());
                }
                else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        requestPermissions(listPermissions, REQUEST_CALL_PHONE_CODE);
                    }
                }
            }
        });

        viewandress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (profile != null) {
                    String map = "http://maps.google.co.in/maps?q=" + profile.getAddress();
                    Intent i = new Intent(Intent.ACTION_VIEW, Uri.parse(map));
                    startActivity(i);
                }
            }
        });
    }

    private boolean checkPermissions(Context context, String[] listPermissions) {
        if (context != null && listPermissions != null) {
            for (String permission : listPermissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    private void ensurePermissions() {
        listPermissions = new String[]{Manifest.permission.READ_EXTERNAL_STORAGE};
        if (checkPermissions(this, listPermissions)) {
            openGalery();
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                requestPermissions(listPermissions, REQUEST_PERMISSIONS_CODE);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == REQUEST_PERMISSIONS_CODE) {
            if (checkPermissions(ProfileActivity.this, listPermissions)) {
                openGalery();
            } else {
                showToast("Yêu cầu bị từ chối!");
            }
        }
        else if (requestCode == REQUEST_CALL_PHONE_CODE){
            if (checkPermissions(ProfileActivity.this, listPermissions)) {
                callphone(profile.getPhone());
            } else {
                showToast("Yêu cầu bị từ chối!");
            }
        }
    }

    private void openGalery() {
        Intent intent = new Intent();
        intent.setAction(Intent.ACTION_PICK);
        intent.setType("image/*");

        startActivityForResult(intent, REQUEST_GET_IMAGE_CODE);
    }

    private void callphone(String phone){
        String uri = "tel:" + phone ;
        Intent intent = new Intent();
        intent.setType(Intent.ACTION_PICK);
        intent.setData(Uri.parse(uri));
        startActivity(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_GET_IMAGE_CODE && resultCode == RESULT_OK && data != null) {
            Uri uri = data.getData();
            Glide.with(ProfileActivity.this).load(uri).into(imv_bigavatar);
            uploadImage(uri);
        }
    }

    private void uploadImage(Uri uri) {
        StorageReference ref = reference.child("avatar" + uri.getLastPathSegment());
        UploadTask uploadTask = ref.putFile(uri);
        uploadTask.continueWithTask(new Continuation<UploadTask.TaskSnapshot, Task<Uri>>() {
            @Override
            public Task<Uri> then(@NonNull Task<UploadTask.TaskSnapshot> task) throws Exception {
                return ref.getDownloadUrl();
            }
        }).addOnCompleteListener(new OnCompleteListener<Uri>() {
            @Override
            public void onComplete(@NonNull Task<Uri> task) {
                if (task.isSuccessful()) {
                    Log.d("nhuthao234", task.getResult().toString());
                    updateAvatar(task.getResult().toString(), userInfo.getId());
                    statusAdapter.notifyDataSetChanged();
                } else {
                    showToast("Đăng ảnh thất bại!");
                }
            }
        });
    }

    private void updateAvatar(String avatarUrl, String userId) {
        retrofitService.updateAvatar(new AvatarUrl(avatarUrl), userId).enqueue(new Callback<AvatarUrl>() {
            @Override
            public void onResponse(Call<AvatarUrl> call, Response<AvatarUrl> response) {
                AvatarUrl avatar = response.body();
                if (response.code() == 200 && avatar != null && avatar.getAvatarUrl() != null) {
                    showToast("Đăng ảnh thành công!");
                } else {
                    showToast("Lỗi xử lý");
                }
            }

            @Override
            public void onFailure(Call<AvatarUrl> call, Throwable t) {
                showToast("Không có mạng");
            }
        });
    }

    private void getProfile() {
        retrofitService.getProfile(username, userInfo.getId()).enqueue(new Callback<Profile>() {
            @Override
            public void onResponse(Call<Profile> call, Response<Profile> response) {
                profile = response.body();
                if (response.code() == 200 && profile != null) {
                    name.setText(profile.getFullName());
                    andress.setText(profile.getAddress());
                    phone.setText(profile.getPhone());
                    birthday.setText(profile.getBirthday());
                    Glide.with(getApplicationContext()).load(profile.getAvatarUrl()).into(imv_bigavatar);
                    statusList.clear();
                    statusList.addAll(profile.getPostList());
                    statusAdapter.notifyDataSetChanged();
                    ArrayList<String> coverPhoto = profile.getCoverPhoto();
                    initSlider(coverPhoto);
                    viewFlipper1.setDisplayedChild(3);
                } else {
                    viewFlipper1.setDisplayedChild(1);
                }
            }

            @Override
            public void onFailure(Call<Profile> call, Throwable t) {
                viewFlipper1.setDisplayedChild(2);
            }
        });
    }

    private void initSlider(ArrayList<String> images) {
        sliderLayout.removeAllSliders();
        for (String image : images) {
            DefaultSliderView defaultSliderView = new DefaultSliderView(this);
            defaultSliderView.image(image);
            sliderLayout.addSlider(defaultSliderView);
        }
        sliderLayout.setPresetTransformer(SliderLayout.Transformer.Tablet);
        sliderLayout.setPresetIndicator(SliderLayout.PresetIndicators.Right_Bottom);
        sliderLayout.setDuration(4000);
    }

    @Override
    public void likeclick(int position) {

    }

    @Override
    public void commentclick(Status status) {

    }

    @Override
    public void shareclick(Status status) {

    }

    @Override
    public void avtarclick(Status status) {

    }

    @Override
    public void nameclick(Status status) {

    }

    @Override
    public void onCreatStatus(String content) {

    }
}
