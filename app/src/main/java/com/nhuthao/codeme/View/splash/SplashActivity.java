package com.nhuthao.codeme.View.splash;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

import com.nhuthao.codeme.Model.response.UserInfo;
import com.nhuthao.codeme.R;
import com.nhuthao.codeme.View.Home.HomeActivity;
import com.nhuthao.codeme.View.authen.AuthenActivity;
import com.nhuthao.codeme.database.RealmContext;

public class SplashActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                UserInfo userInfo;
                userInfo= RealmContext.getInstance().getAll();
                if (userInfo != null){
                    gotoHome();
                    return;
                }
                else
                    gotoAuthen();
            }
        }, 1000);
    }

    private void gotoHome(){
        Intent intent = new Intent(this, HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    private void gotoAuthen(){
        Intent intent = new Intent(this, AuthenActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
}
