package com.nhuthao.codeme.View.adapter;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.nhuthao.codeme.Model.response.Status;
import com.nhuthao.codeme.Model.response.UserInfo;
import com.nhuthao.codeme.R;
import com.nhuthao.codeme.View.ProfileActivity;
import com.nhuthao.codeme.database.RealmContext;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class StatusAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final int TYPE_HEADER = 0;
    private static final int TYPE_STATUS = 1;

    private List<Status> statusList;
    private OnclickABC listener;

    public StatusAdapter(List<Status> statusList, OnclickABC listener) {
        this.statusList = statusList;
        this.listener = listener;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType==TYPE_HEADER){
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_header_status,
                    parent, false);
            return new HeaderViewHolder(view);

        }
        else {
            View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.iteam_status,
                    parent, false);
            return new StatusViewHolder(view);
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof StatusViewHolder){
            ((StatusViewHolder) holder).bindStatusView(statusList.get(position-1), position);
        }
    }



    @Override
    public int getItemCount() {
        return statusList.size()+1;
    }

    public class HeaderViewHolder extends RecyclerView.ViewHolder{

        @BindView(R.id.edt_status)
        EditText edtstatus;

        @BindView(R.id.im_avatar)
        CircleImageView imavatar;

        @BindView(R.id.imv_post)
        ImageView imvpost;

        UserInfo userInfo = RealmContext.getInstance().getAll();

        public HeaderViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            Glide.with(itemView.getContext()).load(userInfo.getAvatar()).into(imavatar);
            AddListener();
        }

        private void AddListener(){
            imvpost.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    String content = edtstatus.getText().toString();
                    if (!content.isEmpty()){
                        listener.onCreatStatus(content);
                        edtstatus.setText("");
                    }

                }
            });

        }

    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0){
            return TYPE_HEADER;
        }
        else {
            return TYPE_STATUS;
        }
    }

    public class StatusViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.iv_circle_item)
        CircleImageView imvavatar;

        @BindView(R.id.tv_name_item)
        TextView tvname;

        @BindView(R.id.tv_date)
        TextView tvdate;

        @BindView(R.id.tv_status)
        TextView tvstatus;

        @BindView(R.id.tv_number_like)
        TextView tvnumberlike;

        @BindView(R.id.tv_number_cmt)
        TextView tvnumbercmt;

        @BindView(R.id.like)
        LinearLayout like;

        @BindView(R.id.comment)
        LinearLayout comment;

        @BindView(R.id.share)
        LinearLayout share;

        @BindView(R.id.im_like)
        ImageView imlike;

        @BindView(R.id.tv_like)
        TextView tvlike;

        Status status;

        int position;

        public StatusViewHolder(@NonNull View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            addListener();
        }

        private void addListener(){
            like.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.likeclick(position);
                }
            });

            comment.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.commentclick(status);
                }
            });
            imvavatar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.avtarclick(status);
                }
            });
            tvname.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    listener.nameclick(status);
                }
            });
        }

        @SuppressLint("SetTextI18n")
        private void bindStatusView(Status status, int position){
            this.status=status;
            this.position= position;
            Glide.with(itemView.getContext()).load(status.getAuthorAvatar()).into(imvavatar);
            tvname.setText(status.getAuthorName());
            tvdate.setText(status.getCreateDate());
            tvnumberlike.setText(Integer.toString(status.getNumberLike()));
            tvnumbercmt.setText(Integer.toString(status.getNumberComment()));
            tvstatus.setText(status.getContent());
            if (status.isLike()){
                Glide.with(imlike.getContext()).load(R.drawable.ic_like).into(imlike);
                tvlike.setTextColor(itemView.getResources().getColor(R.color.colorRed500));
            }
            else {
                imlike.setBackground(imlike.getResources().getDrawable(R.drawable.ic_dont_like));
                tvlike.setTextColor(itemView.getResources().getColor(R.color.colorGrey700));
            }
        }
    }

}
