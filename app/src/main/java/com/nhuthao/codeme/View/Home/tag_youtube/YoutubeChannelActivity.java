package com.nhuthao.codeme.View.Home.tag_youtube;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.nhuthao.codeme.R;
import com.nhuthao.codeme.View.Home.tag_youtube.yoututbeList.YoutubeListActivity;

import butterknife.BindView;
import butterknife.ButterKnife;

public class YoutubeChannelActivity extends AppCompatActivity {

    @BindView(R.id.imv_youtube_list)
    ImageView imvyoutubelist;

    @BindView(R.id.list_view)
    LinearLayout listview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_youtube_channel);
        ButterKnife.bind(this);
        init();
        addListner();
    }

    private void init() {

    }

    private void addListner(){
        imvyoutubelist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoList();
            }
        });

        listview.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoList();
            }
        });
    }

    private void gotoList(){
        Intent intent = new Intent(this, YoutubeListActivity.class);
        startActivity(intent);
    }
}
