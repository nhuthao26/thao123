package com.nhuthao.codeme.View.authen;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.nhuthao.codeme.Model.request.RegisterSendForm;
import com.nhuthao.codeme.Model.response.UserInfo;
import com.nhuthao.codeme.R;
import com.nhuthao.codeme.View.authen.AuthenActivity;
import com.nhuthao.codeme.network.RetrofitService;
import com.nhuthao.codeme.network.RetrofitUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddActivity extends AppCompatActivity {

    @BindView(R.id.edt_taikhoan)
    EditText edt_taikhoan;

    @BindView(R.id.edt_matkhau)
    EditText edt_matkhau;

    @BindView(R.id.edt_xacnhanmk)
    EditText edt_xacnhanmk;

    @BindView(R.id.edt_hoten)
    EditText edt_hoten;

    @BindView(R.id.edt_email)
    EditText edt_email;

    @BindView(R.id.edt_sdt)
    EditText edt_sdt;

    @BindView(R.id.bt_dangky)
    Button bt_dangky;

    RetrofitService retrofitService;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add);

        ButterKnife.bind(this);
        addListener();
    }

    private void addListener() {
        bt_dangky.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String username = edt_taikhoan.getText().toString();
                String password = edt_matkhau.getText().toString();
                String repassword = edt_xacnhanmk.getText().toString();
                String email = edt_email.getText().toString();
                String fullName = edt_hoten.getText().toString();
                String phone = edt_sdt.getText().toString();
                if (username.isEmpty()){
                    showToast("Bạn chưa nhập tài khoản");
                    return;
                }
                if (password.isEmpty()){
                    showToast("Bạn chưa nhập mật khẩu");
                    return;
                }
                if (email.isEmpty()){
                    showToast("Bạn chưa nhập email");
                    return;
                }
                if (fullName.isEmpty()){
                    showToast("Bạn chưa nhập họ tên");
                    return;
                }
                if (phone.isEmpty()){
                    showToast("Bạn chưa nhập số điện thoại");
                    return;
                }
                if (!password.equals(repassword)){
                    showToast("Xác nhận mật khẩu chưa chính xác");
                    return;
                }
                retrofitService = RetrofitUtils.getIntance().createService();
                RegisterSendForm sendForm = new RegisterSendForm(username, password,email, fullName, phone);
                retrofitService.Register(sendForm).enqueue(new Callback<UserInfo>() {
                    @Override
                    public void onResponse(Call<UserInfo> call, Response<UserInfo> response) {
                        if (response.code()==200){
                            showToast("Đăng ký thành công!");
                            gotoAuthen();
                        }
                        else {
                            showToast("Tài khoản đã tồn tại");
                        }
                    }

                    @Override
                    public void onFailure(Call<UserInfo> call, Throwable t) {
                        showToast("Lỗi đường truyền, vui lòng kiểm tra lại kết nối Internet của bạn");
                    }
                });

            }
        });
    }

    private void showToast(String message){
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    private void gotoAuthen(){
        Intent intent = new Intent(this, AuthenActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }
}
