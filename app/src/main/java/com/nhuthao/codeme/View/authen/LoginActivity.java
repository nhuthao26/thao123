package com.nhuthao.codeme.View.authen;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.nhuthao.codeme.Model.request.LoginSendForm;
import com.nhuthao.codeme.Model.response.UserInfo;
import com.nhuthao.codeme.R;
import com.nhuthao.codeme.View.Home.HomeActivity;
import com.nhuthao.codeme.View.authen.AddActivity;
import com.nhuthao.codeme.database.RealmContext;
import com.nhuthao.codeme.network.RetrofitService;
import com.nhuthao.codeme.network.RetrofitUtils;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class LoginActivity extends AppCompatActivity {

    @BindView(R.id.edt_taikhoan)
    EditText edttaikhoan;

    @BindView(R.id.edt_matkhau)
    EditText edtmatkhau;

    @BindView(R.id.bt_dangnhap)
    Button btdangnhap;

    @BindView(R.id.linear_dangky)
    LinearLayout lineardangky;

    RetrofitService retrofitService;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);


        retrofitService = RetrofitUtils.getIntance().createService();
        addListener();
    }

    @Override
    protected void onResume() {
        super.onResume();

        edtmatkhau.setText("");

    }

    private  void addListener(){

        btdangnhap.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String username = edttaikhoan.getText().toString();
                String password = edtmatkhau.getText().toString();
                if (username.isEmpty()||password.isEmpty()){
                    showToast("Bạn phải nhập đầy đủ tài khoản và mật khẩu!");
                }
                else {
                    Login(username, password);

                }
            }
        });

        lineardangky.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoAdd();
            }
        });
    }


    private void gotoAdd(){
        Intent intent = new Intent(this, AddActivity.class);
        startActivity(intent);
    }

    private void Login(String username, String password){
        LoginSendForm sendForm = new LoginSendForm(username, password);
        retrofitService.login(sendForm).enqueue(new Callback<UserInfo>() {
            @Override
            public void onResponse(Call<UserInfo> call, Response<UserInfo> response) {
                if(response.code()==200){
                    UserInfo userInfo = response.body();
                    //save userinfo vao realm
                    RealmContext.getInstance().add(userInfo);
                    gotoHome();
                }
                else {
                    showToast("Tài khoản hoặc mật khẩu không chính xác!");
                }
            }

            @Override
            public void onFailure(Call<UserInfo> call, Throwable t) {
                showToast("Lỗi đường truyền, vui lòng kiểm tra lại kết nối Internet của bạn");
            }
        });
    }

    private void showToast(String message){
        Toast.makeText(this,message, Toast.LENGTH_SHORT).show();
    }

    private void gotoHome(){
        Intent intent = new Intent(this, HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }
}
