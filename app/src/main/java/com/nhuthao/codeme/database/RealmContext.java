package com.nhuthao.codeme.database;

import com.nhuthao.codeme.Model.response.UserInfo;

import io.realm.Realm;

public class RealmContext {

    private static RealmContext instance;

    private Realm realm;

    public static RealmContext getInstance(){
        if (instance==null){
            instance = new RealmContext();
        }
        return instance;
    }

    private RealmContext(){
        realm = Realm.getDefaultInstance();
    }

    public void add(UserInfo userInfo){

        Number max = realm.where(UserInfo.class).max("stt");

        if (max == null){
            userInfo.setStt(1);
        }
        else {
            userInfo.setStt(max.intValue()+1);
        }
        realm.beginTransaction();
        realm.insertOrUpdate(userInfo);
        realm.commitTransaction();
    }

    public UserInfo getAll(){
        return realm.where(UserInfo.class).findFirst();
    }

    private UserInfo getS(int stt){
        return realm.where(UserInfo.class).equalTo("stt", stt).findFirst();
    }

//    public void update(UserInfo userInfo){
//        realm.beginTransaction();
//        realm.insertOrUpdate(userInfo);
//        realm.commitTransaction();
//    }

    public void delete(UserInfo userInfo){
        realm.beginTransaction();
        userInfo.deleteFromRealm();
        realm.commitTransaction();
    }

}
